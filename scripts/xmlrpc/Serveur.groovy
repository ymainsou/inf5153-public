@Grab('org.codehaus.groovy:groovy-xmlrpc:0.8')

import groovy.net.xmlrpc.*
import java.net.ServerSocket

def server = new XMLRPCServer()

server.echo = { it }

server.majuscule = { it.toUpperCase() }

server.addition = { a, b -> a + b }

def serverSocket = new ServerSocket( 8080 )
server.startServer(serverSocket)
