@Grab('org.codehaus.groovy:groovy-xmlrpc:0.8')

import groovy.net.xmlrpc.*
import java.time.*

def url = "http://localhost:8080"
def serverProxy = new XMLRPCServerProxy(url)

println serverProxy.echo("Bonjour le monde !")
println serverProxy.majuscule("ça fonctionne !")
println serverProxy.addition( 3, 4 )

// Test RPC sur le même ordinateur

def chronometrer = { bloc ->
    def avant = Instant.now()
    bloc()
    def apres = Instant.now()
    println "Durée en millisecondes : ${Duration.between(avant, apres).toMillis()}"
}

chronometrer { 1000.times { serverProxy.addition( 5, 9 ) } }

def addition = { a, b -> a + b }
chronometrer { 3000000.times { addition( 5, 9 ) } }
