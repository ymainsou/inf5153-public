@Grapes( [
    @Grab('io.ratpack:ratpack-groovy:1.4.2'),
    @Grab('org.slf4j:slf4j-simple:1.7.21'),
] )

import static ratpack.groovy.Groovy.ratpack

ratpack {
    handlers {
        get {
            render 'Bienvenue sur mon site Ratpack\n'
        }
        get('salutation') {
            def nom = request.queryParams.nom ?: "le monde"
            render "Bonjour $nom !\n"
        }
    }
}
