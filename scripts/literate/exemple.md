% Petit tutoriel Groovy
% Louis Martin
% \today
\clearpage

# Introduction

Petit tutoriel Groovy pour des développeurs Java.

Ce tutoriel est réalisé avec le concept de la programmation lettrée (*literate programming*) (voir <https://fr.wikipedia.org/wiki/Programmation_lettr%C3%A9e>) quoique pour les puristes, ce n'en soit pas. En fait, le fichier source incluant les commentaires est exécutable directement en Groovy. Le même fichier avec une transformation minime permet de générer un document PDF avec pandoc.

# Liens utiles

- <http://groovy-lang.org/> Site principal de Groovy

- <https://groovy-playground.appspot.com/> Terrain de jeu pour Groovy

- <https://groovyconsole.appspot.com/> Un autre terrai de jeu pour Groovy

- <http://beakernotebook.com/> Beaker Notebook supporte Groovy, ce logiciel est similaire à Jupyter <http://jupyter.org/> connu dans l'univers Python

# Importation et définition des variables pour le présent script

```java
import static groovy.test.GroovyAssert.shouldFail

def test = {}
```

# Bonjour le monde !

Le fameux exemple de *Bonjour le monde !*
Voici l'exemple en Java :

```java
public class Bonjour {

    public static void main(String[] args) {
        System.out.println('Bonjour le monde !');
    }

}
```

Et l'équivalent en Groovy :

```java
println 'Bonjour le monde !'
```

# Différences entre Java et Groovy

Références utiles :

- <http://groovy-lang.org/semantics.html>.
- <http://groovy-lang.org/structure.html>

## Les facilitateurs

- Les points-virgules sont optionnels
- Les exceptions ne doivent pas être traitées explicitement
- Les imports automatiques :
     - `import java.lang.*`
     - `import java.util.*`
     - `import java.io.*`
     - `import java.net.*`
     - `import groovy.lang.*`
     - `import groovy.util.*`
     - `import java.math.BigInteger`
     - `import java.math.BigDecimal`

## Les chaines de charactères et les *GStrings*

Les chaines de caractères en Groovy peuvent s'étendre sur plusieurs lignes

```java
assert '''
a
b
''' == '\na\nb\n'

assert '''\
    a
    b
    '''.stripIndent() == 'a\nb\n'
```

Les *GStrings* agissent comme un gabarit

```java
test = {
    def nom = 'Albert Einstein'
    def gabarit = "Bonjour Monsieur $nom"
    def autreGabarit = "Bonjour Monsieur ${nom.toUpperCase()}"
    assert gabarit == 'Bonjour Monsieur Albert Einstein'
    assert autreGabarit == 'Bonjour Monsieur ALBERT EINSTEIN'
}()
```

## Le *vrai* selon Groovy

Les booléens sont comme en Java.

```java
assert true
assert !false
```

Les tableaux et les collections non vides sont vrais.

```java
assert [1,2,3]
assert ![]
```

Les dictionnaires (*maps*) non vides sont vrais.

```java
assert ['un': 1]
assert ![:]
```

Un expression régulière correspondant au moins une fois à une chaine de caractères est vraie.

```java
assert ('a' =~ /a/)
assert !('a' =~ /b/)
```

Les itérateurs et les énumérations ayant encore des éléments sont vrais.

```java
test = {
    assert [0].iterator()
    assert ![].iterator()
    Vector v = [0] as Vector
    Enumeration enumeration = v.elements()
    assert enumeration
    enumeration.nextElement()
    assert !enumeration
}()
```

Les *Strings*, *GStrings* et *CharSequences* non vides sont vrais.

```java
test = {
    assert 'a'
    assert !''
    def nonVide = 'a'
    assert "$nonVide"
    def vide = ''
    assert !"$vide"
}()
```

Les nombres différents de zéro sont vrais.

```java
assert 1
assert 3.5
assert !0
```

Les références objet non *null* sont vraies.

```java
assert new Object()
assert !null
```

## Les boucles et les *ranges*

Les boucles de type `for` sont les mêmes qu'en Java.
De plus Groovy propose des approches intéressantes.

Exécuter un bloc un certain nombre de fois

```java
10.times { n ->
    assert n in 0..9
}
```

Ou si on utilise la variable par défaut `it`

```java
10.times { assert it in 0..9 }
```

Groovy supporte les *ranges*

```java
assert (1..10).inject(0) { somme, it -> somme + it } == 55

assert (1..<10).inject(0) { somme, it -> somme + it } == 45
```

Avec `collect` et `findAll` construisons la liste des carrés des nombres pairs de 1 à 10

```java
assert (1..10).findAll({ it % 2 == 0 }).collect { it**2 } == [4, 16, 36, 64, 100]
```

## Les *closures*

Voir <http://groovy-lang.org/closures.html>

> This chapter covers Groovy Closures. A closure in Groovy is an open, anonymous, block of code that can take arguments, return a value and be assigned to a variable. A closure may reference variables declared in its surrounding scope. In opposition to the formal definition of a closure, Closure in the Groovy language can also contain free variables which are defined outside of its surrounding scope. While breaking the formal concept of a closure, it offers a variety of advantages which are described in this chapter.

# Binding

Ne s'applique qu'aux scripts.

Voir <http://www.justinleegrant.com/?p=129)>.

Voir aussi <http://groovy.jmiguel.eu/groovy.codehaus.org/Scoping+and+the+Semantics+of+%22def%22.html>.

```java
test = {
    nomA = 'Louis' // mis dans le binding
    def nomB = 'Martin' // visibilité locale seulement

    assert binding.variables.containsKey('nomA') == true
    assert binding.variables.get('nomA') == 'Louis'

    assert binding.variables.containsKey('nomB') == false
    assert binding.variables.get('nomB') == null
}()

test = {
    assert nomA == 'Louis'
}()

test = {
    shouldFail {
        assert nomB == 'Martin'
    }
}()
```

# Méta-programmation

Groovy permet la méta-programmation et facilite la création de langages dédiés (*DSL -- Domain Specific Language*).

Voir <http://docs.groovy-lang.org/docs/latest/html/documentation/core-domain-specific-languages.html>.
