@Grapes( [
   @Grab( 'org.xerial:sqlite-jdbc:3.14.2.1' ),
   @GrabConfig( systemClassLoader=true ),
] )

import groovy.sql.Sql

def retraitTablePersonnes = '''
    DROP TABLE IF EXISTS personnes;
'''

def creationTablePersonnes = '''
    CREATE TABLE personnes (
        prenom TEXT,
        nomFamille TEXT
    );
'''

def creationPersonne = '''
    INSERT INTO personnes (prenom, nomFamille) VALUES (?, ?);
'''

def selectionPersonnes = '''
    SELECT prenom, nomFamille FROM personnes;
'''

def personnes = [
    [ 'Louis', 'Martin' ],
    [ 'Albert', 'Einstein' ],
    [ 'Marie', 'Curie' ],
    [ 'Joe', 'Louis' ],
    [ 'Marilyn', 'Monroe' ],
]

Sql.withInstance('jdbc:sqlite:test.sqlite', 'org.sqlite.JDBC') { sql ->

    // Un énoncé SQL à la fois avec Sqlite3
    sql.execute retraitTablePersonnes
    sql.execute creationTablePersonnes

    personnes.each { personne ->
        def cle = sql.executeInsert creationPersonne, personne
        println cle
    }

    println "\nListe des personnes\n"
    sql.eachRow selectionPersonnes, { rangee ->
        println "$rangee.prenom $rangee.nomFamille"
    }

}
