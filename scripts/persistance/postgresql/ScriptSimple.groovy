// Au préalable, le serveur de base de données PostgreSQL doit être lancé

@Grapes( [
    @Grab('org.postgresql:postgresql:9.4.1211.jre7'),
    @Grab('commons-dbcp:commons-dbcp:1.4'),
    @GrabConfig(systemClassLoader=true),
] )

import groovy.sql.Sql
import org.apache.commons.dbcp.BasicDataSource

def dataSource = new BasicDataSource(
    url: 'jdbc:postgresql://localhost/louis',
    username: 'louis',
    password: '',
    driverClassName: 'org.postgresql.Driver'
)

def creationTablePersonnes = '''
    DROP TABLE IF EXISTS personnes;
    CREATE TABLE personnes (
        id SERIAL PRIMARY KEY,
        prenom VARCHAR(64),
        nomFamille VARCHAR(64)
    );
'''

def creationPersonne = '''
    INSERT INTO personnes (prenom, nomFamille) VALUES (?, ?);
'''

def selectionPersonnes = '''
    SELECT prenom, nomFamille FROM personnes;
'''

def personnes = [
    [ 'Louis', 'Martin' ],
    [ 'Albert', 'Einstein' ],
    [ 'Marie', 'Curie' ],
    [ 'Joe', 'Louis' ],
    [ 'Marilyn', 'Monroe' ],
]

def sql = new Sql(dataSource)

sql.execute creationTablePersonnes

personnes.each { personne ->
    def cle = sql.executeInsert creationPersonne, personne
    println cle
}

println "\nListe des personnes\n"
sql.eachRow selectionPersonnes, { rangee ->
    println "$rangee.prenom $rangee.nomFamille"
}

sql.close()
