# LISEZ-MOI

### Pourquoi ce dépôt ?

- Pour contenir quelques exemples de code en [Groovy](http://www.groovy-lang.org/).

## Répertoire `scripts`

Ce répertoire contient des scripts *Groovy* exécutables directement.

- Le sous-répertoire `literate` contient un script et un exemple permettant d'approcher le concept de *literate programming* proposé par [Donald Knuth](https://fr.wikipedia.org/wiki/Programmation_lettr%C3%A9e). Le script est dépendant de [Pandoc](http://pandoc.org/) et de [LaTeX](https://www.latex-project.org/).

- Le sous-répertoire `persistance` contient des exemples avec les bases de données suivantes :

    - [HSQLDB](http://hsqldb.org/)
    - [PostgreSQL](https://www.postgresql.org/) (nécessite l'installation préalable de PostgreSQL)
    - [SQLite](https://sqlite.org/)

- Le sous-répertoire `ratpack` contient des exemples simples avec le cadriciel [Ratpack](https://ratpack.io/). Ratpack permet de développer des microservices avec le protocole [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), le patron de conception [REST](https://fr.wikipedia.org/wiki/Representational_state_transfer) utilisant entre autres le format [JSON](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation).

- Le sous-répertoire `tp1` contient une solution possible au TP1.

- Le sous-répertoire `xmlrpc` contient un serveur et un client utilisant le protocole XML-RPC pour communiquer.

## Répertoire `src/main/groovy`

Ce répertoire utilise *Gradle*. Ce logiciel doit être
préinstallé et paramétré correctement.

- Le sous-répertoire `patterns` contient des exemples de quelques patrons de conception.

## Répertoire `src/test/groovy`

Ce répertoire associé au précédent contient des tests. Les tests utilisent le cadriciel (*framework*) [Spock](http://spockframework.org/).
