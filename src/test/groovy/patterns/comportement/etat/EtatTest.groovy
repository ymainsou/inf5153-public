package patterns.comportement.etat

import spock.lang.Specification

class EtatTest extends Specification {

    def 'Compte avec état régulier et un dépôt'() {
        given: 'Un compte avec état régulier'
            def compte = new Compte("001")
        when: 'Un dépôt de 100 $'
            compte.deposer(100.00)
        then: 'Le solde est de 100 $'
            compte.solde == 100.00
    }

    def 'Compte avec état régulier et un retrait'() {
        given: 'Un compte avec état régulier'
            def compte = new Compte("001")
        when: 'Un dépôt de 100 $'
            compte.deposer(100.00)
        and: 'Un retrait de 20 $'
            compte.retirer(20.00)
        then: 'Le solde est de 80 $'
            compte.solde == 80.00
    }

    def 'Compte avec état décès et un dépôt'() {
        given: 'Un compte'
            def compte = new Compte("001")
        when: "L'état est 'décès'"
            compte.setEtatDeces()
        and: 'Un dépôt de 20 $'
            compte.deposer(20.00)
        then: 'Le solde est de 20 $'
            compte.solde == 20.00
    }

    def 'Compte avec état décès et un retrait'() {
        given: 'Un compte'
            def compte = new Compte("001")
        when: "L'état est 'décès'"
            compte.setEtatDeces()
        and: 'Un dépôt de 20 $'
            compte.deposer(20.00)
        and: 'Un retrait de 10 $'
            compte.retirer(10.00)
        then: 'Une exception est lancée car les retraits ne sont pas permis dans cet état'
            def ex = thrown(Exception)
            ex.message == 'Opération non permise'
            compte.solde == 20.00
    }

    def 'Compte avec état restreint et un retrait inférieur à 25 $'() {
        given: 'Un compte'
            def compte = new Compte("001")
        when: "L'état est 'restreint'"
            compte.setEtatRestreint()
        and: 'Un dépôt de 40 $'
            compte.deposer(40.00)
        and: 'Un retrait de 10 $'
            compte.retirer(10.00)
        then: 'Le solde est de 30 $'
            compte.solde == 30.00
    }

    def 'Compte avec état restreint et un retrait supérieur à 25 $'() {
        given: 'Un compte'
            def compte = new Compte("001")
        when: "L'état est 'restreint'"
            compte.setEtatRestreint()
        and: 'Un dépôt de 80 $'
            compte.deposer(80.00)
        and: 'Un retrait de 30 $'
            compte.retirer(30.00)
        then: 'Une exception est lancée car les retraits supérieurs à 25 $ ne sont pas permis dans cet état'
            def ex = thrown(Exception)
            ex.message == 'Opération non permise'
            compte.solde == 80.00
    }

}
