package patterns.creation.fabrique

import spock.lang.Specification

class FabriquePersonneTest extends Specification {

    def "Créer une personne morale"() {
        given: "Un nom et un type d'organisation valide"
            def google = FabriquePersonne.creer("Google", "COMPAGNIE")
        expect: "Une personne morale et non une personne physique"
            google.estPersonneMorale()
            !google.estPersonnePhysique()
    }

    def "Créer une personne physique"() {
        given: "Un nom de famille, un prénom et un string représentant une date de naissance"
            def albert = FabriquePersonne.creer(
                "Einstein",
                "Albert",
                "1879-03-14"
            )
        expect: "Une personne physique et non une personne morale"
            albert.estPersonnePhysique()
            !albert.estPersonneMorale()
    }

    def "Créer une personne morale avec paramètres nommés"() {
        given: "Un paramètre 'nom' et un paramètre 'type' valide"
            def csn = FabriquePersonne.creer(nom:"CSN", type:"SYNDICAT")
        expect: "Une personne morale"
            csn.estPersonneMorale()
            csn.nom == 'CSN'
            csn.type == TypePersonneMorale.SYNDICAT
    }

    def "Créer une personne physique avec paramètres nommés"() {
        given: "Les paramètres 'nomFamille', 'prenom' et 'dateNaissance'"
            def oscar = FabriquePersonne.creer(
                prenom:"Oscar",
                dateNaissance: "1925-08-15",
                nomFamille: "Peterson"
            )
            def dateTemoin = Date.parse('yyyy-MM-dd', '1925-08-15')
        expect: "Une personne physique"
            oscar.prenom == 'Oscar'
            oscar.nomFamille == 'Peterson'
            oscar.dateNaissance == dateTemoin
    }

    def "Appel invalide à la fabrique avec paramètres nommés"() {
        given: "Au moins un des paramètres requis absent, ici la date de naissance"
            def bond = FabriquePersonne.creer(
                prenom:"James",
                nomFamille: "Bond"
            )
        expect: "null"
            bond == null
    }

    def "Appel invalide à la fabrique avec paramètres nommés"() {
        given: "Des paramètres nommés non pertinents ici le paramètre 'toto' est le seul paramètre utilisé"
            def toto = FabriquePersonne.creer(toto:"moi")
        expect: "null"
            toto == null
    }

}
