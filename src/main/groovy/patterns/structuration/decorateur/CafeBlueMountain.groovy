package patterns.structuration.decorateur

class CafeBlueMountain implements Cafe {

    {
        description = "Café BlueMountain"
        prix = 2.25
    }

}
