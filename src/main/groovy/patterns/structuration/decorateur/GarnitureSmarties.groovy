package patterns.structuration.decorateur

class GarnitureSmarties extends Garniture {

    GarnitureSmarties(Cafe cafe) {
        super(cafe, "Smarties", 0.40)
    }

}
