package patterns.structuration.decorateur

class GarnitureCreme extends Garniture {

    GarnitureCreme(Cafe cafe) {
        super(cafe, "crème fouettée", 0.30)
    }

}
