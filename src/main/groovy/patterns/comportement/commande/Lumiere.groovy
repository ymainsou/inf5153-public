package patterns.comportement.commande

class Lumiere {

    def allumer() {
        println 'Lumière allumée'
    }

    def eteindre() {
        println 'Lumière éteinte'
    }

}
