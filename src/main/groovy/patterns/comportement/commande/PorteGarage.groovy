package patterns.comportement.commande

class PorteGarage {

    def ouvrir() {
        println 'Porte de garage ouverte'
    }

    def fermer() {
        println 'Porte de garage fermée'
    }

}
