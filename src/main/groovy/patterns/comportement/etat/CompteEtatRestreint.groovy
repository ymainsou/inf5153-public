package patterns.comportement.etat

class CompteEtatRestreint extends CompteEtat {

    CompteEtatRestreint(Compte compte) {
        super(compte)
    }

    def retirer(BigDecimal montant) {
        if (montant > 25.00) {
            throw new Exception("Opération non permise")
        }
        super.retirer montant
    }

}
