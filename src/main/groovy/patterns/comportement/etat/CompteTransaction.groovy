package patterns.comportement.etat

interface CompteTransaction {

    def deposer(BigDecimal montant)

    def retirer(BigDecimal montant)

}
