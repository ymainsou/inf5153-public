package patterns.comportement.etat

class CompteEtatDeces extends CompteEtat {

    CompteEtatDeces(Compte compte) {
        super(compte)
    }

    def retirer(BigDecimal montant) {
        throw new Exception("Opération non permise")
    }

}
