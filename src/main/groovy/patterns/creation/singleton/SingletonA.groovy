package patterns.creation.singleton

@Singleton
class SingletonA {

    String toString() { "Je suis un singletonA " + super.toString() }

}
