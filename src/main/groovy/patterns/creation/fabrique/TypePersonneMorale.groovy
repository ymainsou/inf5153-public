package patterns.creation.fabrique

enum TypePersonneMorale {

    COMPAGNIE("Société par actions"),
    SENC("Société en nom collectif"),
    SEC("Société en commandite"),
    OSBL("Personne morale sans but lucratif"),
    COPROPRIETE("Syndicat de copropriété"),
    COOPERATIVE("Coopérative"),
    FIDUCIE("Fiducie exploitant une entreprise à caractère commercial"),
    ASSOCIATION("Association ou groupement de personnes"),
    SYNDICAT("Syndicat professionnel"),
    PUBLIQUE("Société d'État ou société publique")

    TypePersonneMorale(String description) { this.description = description }

    final description

}
