package patterns.creation.fabrique

class PersonnePhysique extends Personne {

    String prenom
    String nomFamille
    Date dateNaissance

    PersonnePhysique(String nomFamille, String prenom, Date dateNaissance) {
        this.nomFamille = nomFamille
        this.prenom = prenom
        this.dateNaissance = dateNaissance
    }

    String getNom() { "${nomFamille?.toUpperCase()}, $prenom" }

    String getDateNaissanceAsString() { dateNaissance.format("dd MMMMM yyyy") }

    String toString() {
        "${super.toString()}Date de naissance : $dateNaissanceAsString\n\t"
    }

}
