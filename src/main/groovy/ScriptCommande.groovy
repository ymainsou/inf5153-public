import patterns.comportement.commande.*

// Instanciation des objets
def alarme = new Alarme()
def lumiere = new Lumiere()
def porteGarage = new PorteGarage()
def telecommande = new Telecommande()

// Définition des macros
def arriver = { ->
    alarme.desarmer()
    lumiere.allumer()
    porteGarage.ouvrir()
}
def quitter = { ->
    porteGarage.fermer()
    alarme.armer()
}

// Programme de la télécommande
def programme = [
    a_ON: alarme.&armer,
    a_OFF: alarme.&desarmer,
    l_ON: lumiere.&allumer,
    l_OFF: lumiere.&eteindre,
    p_ON: porteGarage.&ouvrir,
    p_OFF: porteGarage.&fermer,
    arriver: arriver,
    quitter: quitter,
]

// Chargement du programme dans la télécommande
programme.each { bouton, commande ->
    telecommande.charger bouton, commande
}

// Test des boutons
telecommande.with {
    clic 'a_ON'
    clic 'a_OFF'
    clic 'l_ON'
    clic 'l_OFF'
    clic 'p_ON'
    clic 'p_OFF'
    clic 'arriver'
    clic 'quitter'
    clic 'xyz'
}
